package main

import "testing"

func TestLeftShift(t *testing.T) {
	tables := []struct {
		current_o string
		next_o string
	}{
		{"N", "W"},
		{"W", "S"},
		{"S", "E"},
		{"E", "N"},
	}
	for _, table := range tables {
		next_o := leftShift(table.current_o)
		if next_o != table.next_o {
			t.Errorf("Res was incorrect, got: %v, want: %v.", next_o, table.next_o)
		}
	}
}

func TestRightShift(t *testing.T) {
	tables := []struct {
		current_o string
		next_o string
	}{
		{"N", "E"},
		{"E", "S"},
		{"S", "W"},
		{"W", "N"},
	}
	for _, table := range tables {
		next_o := rightShift(table.current_o)
		if next_o != table.next_o {
			t.Errorf("Res was incorrect, got: %v, want: %v.", next_o, table.next_o)
		}
	}
}

func TestGetlawnSize(t *testing.T) {
	tables := []struct {
		firstLine string
		lawnAxes map[string]int
	}{
		{"5 5", map[string]int{"x": 5,"y":5} },
		{"100 100", map[string]int{"x": 100,"y":100} },
		{"0 0", map[string]int{"x": 0,"y":0} },
	}
	for _, table := range tables {
		lawnAxes := getlawnSize(table.firstLine)
		if lawnAxes["x"] != table.lawnAxes["x"] || lawnAxes["y"] != table.lawnAxes["y"]{
			t.Errorf("Res was incorrect, got: %v, want: %v.", lawnAxes, table.lawnAxes)
		}
	}
}

func TestSetNewMower(t *testing.T) {
	tables := []struct {
		line string
		mower MowerData
	}{
		{"1 2 N", MowerData{x: 1, y:2, orientation:"N"} },
		{"3 3 E", MowerData{x: 3, y:3, orientation:"E"} },
	}
	for _, table := range tables {
		mower := setNewMower(table.line)
		if mower.x != table.mower.x || mower.y != table.mower.y{
			t.Errorf("Res was incorrect, got: %v, want: %v.", mower, table.mower)
		}
	}
}

func TestRunMower(t *testing.T) {
	lawnAxes := map[string]int{"x": 5,"y":5}
	commands := map[int][]string{
		0:{"L","F","L","F","L","F","L","F","F"},
		1:{"F","F","R","F","F","R","F","R","R","F"},
	}
	tables := []struct {
		res_x int
		res_y int
		res_o string
		mower MowerData
	}{
		{1, 3, "N", MowerData{x: 1, y: 2, orientation:"N", c:make(chan string, len(commands[0]))}},
		{5, 1, "E", MowerData{x: 3, y: 3, orientation:"E", c:make(chan string, len(commands[1]))}},
	}
	mowers_slice := []MowerData{}
	p := &mowers_slice
	for id, v := range tables {
		wgChanCMDs.Add(1)
		go createChanCMDs(commands[id], v.mower.c, &wgChanCMDs)
	}
	wgChanCMDs.Wait()
	for _, v := range tables {
		mowers_slice = append(mowers_slice, v.mower)
	}
	for id := range tables {
		wgMowers.Add(1)
		go runMower(id, lawnAxes, p, &wgMowers)
	}
	wgMowers.Wait()
	for id, v := range tables {
		if (*p)[id].x != v.res_x || (*p)[id].y != v.res_y || (*p)[id].orientation != v.res_o {
			t.Errorf("Res was incorrect, got: %v, want: %v.", (*p)[id], v.mower)
		}
	}
}


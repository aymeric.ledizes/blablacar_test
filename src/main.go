package main

import (
    "bufio"
    "fmt"
    "os"
    "log"
    "strings"
    "math"
    "strconv"
    "sync"
    //"time" //To check multi-threads go routines actions -> uncomment time.Sleep lines
)

type MowerData struct {
x int
y int
orientation string
c chan string
mux *sync.Mutex
}

var wgChanCMDs sync.WaitGroup
var wgMowers sync.WaitGroup

func getlawnSize(firstLine string) map[string]int{
    lawnSize := strings.Split(firstLine, " ")
    if len(lawnSize) != 2{
        log.Fatal("err: bad input file"); os.Exit(1)
    }
    lawnWidth, err := strconv.Atoi(lawnSize[0])
    if err != nil {fmt.Println(err)}
    lawnHeight, err := strconv.Atoi(lawnSize[1])
    if err != nil {fmt.Println(err)}
    lawnAxes := map[string]int{"x": lawnWidth,"y": lawnHeight}
    return lawnAxes
}

func setNewMower(line string)(MowerData){
    positionVals := strings.Split(line, " ")
    if len(positionVals) != 3 {
        log.Fatal("err: bad file input"); os.Exit(1)
    }
    initPosition := map[string]string{"x": positionVals[0], "y": positionVals[1], "o": positionVals[2]}
    x, err := strconv.Atoi(initPosition["x"])
    if err != nil {fmt.Println(err)}
    y, err := strconv.Atoi(initPosition["y"])
    if err != nil {fmt.Println(err)}
    mower := MowerData{}
    mower.x, mower.y = x, y
    mower.orientation = initPosition["o"]
    return mower
}

func readData() ([]MowerData, map[string]int){ 
    mowers_slice := []MowerData{}
    lawnAxes := map[string]int{}
    file, err := os.Open("../input")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    lineCount, mowerCount := 0, 0
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        if lineCount == 0 {
            firstLine := scanner.Text()
            lawnAxes = getlawnSize(firstLine)

        } else if math.Mod(float64(lineCount), 2) == 1 {
            line := (scanner.Text())
            mower := setNewMower(line)
            mowers_slice = append(mowers_slice, mower)

        } else {
            //push commands via go routines on a separate channel for each mower
            line := (scanner.Text())
            commands := strings.Split(line, "") //For a next improvement check how to read and push direcly in a channels without passing through a slice
            mowers_slice[mowerCount].c = make(chan string, len(commands))
            wgChanCMDs.Add(1)
            go createChanCMDs(commands, mowers_slice[mowerCount].c, &wgChanCMDs)
            mowerCount++
        }
        lineCount++
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    return mowers_slice, lawnAxes
}

func leftShift(current_O string) string{
        var next_O string
        switch current_O {
        case "N":
            next_O = "W"
        case "W":
            next_O = "S"
        case "S":
            next_O = "E"
        case "E":
            next_O = "N"
        default:
            next_O = current_O
        }
        return next_O
}

func rightShift(current_O string) string{
        var next_O string
        switch current_O {
        case "N":
            next_O = "E"
        case "E":
            next_O = "S"
        case "S":
            next_O = "W"
        case "W":
            next_O = "N"
        default:
            next_O = current_O
        }
        return next_O
}

func createChanCMDs(commands []string, c chan string, wg *sync.WaitGroup) {
    defer wg.Done()
    for _, v := range commands {
    //time.Sleep(100 * time.Millisecond)
    fmt.Println("Push command in the mower channel +++",v)
    c <- v
    }
    fmt.Println("Done: closing channel")
    close(c)
}

func runMower(id int, lawnAxes map[string]int, p *[]MowerData, wg *sync.WaitGroup){
    defer wg.Done()
    c := (*p)[id].c
    (*p)[id].mux = &sync.Mutex{}
    for nextMove := range c {
        fmt.Print("Goroutine/mower:",id,"\n",(*p)[id].x,(*p)[id].y," ",(*p)[id].orientation,"\n\n")
        //time.Sleep(100 * time.Millisecond)
            //Switch cases to know the new desired state
        current_O := (*p)[id].orientation
        if nextMove == "L"{
            (*p)[id].orientation = leftShift(current_O)
            continue

        } else if nextMove == "R"{
            (*p)[id].orientation = rightShift(current_O)
            continue

        } else if nextMove == "F"{
            _x := (*p)[id].x  //current x
            _y := (*p)[id].y  //current y
            var outOfLawn bool
            var busySpace bool
            switch current_O {
            case "N":
                if _y >= lawnAxes["y"] {outOfLawn = true
                } else {
                    _y = _y + 1
                }
            case "S":
                if _y <= 0 {outOfLawn = true
                } else {
                    _y = _y - 1
                }
            case "W":
                if _x <= 0 {outOfLawn = true
                } else {
                    _x = _x - 1
                }
            case "E":
                if _x >= lawnAxes["x"] {outOfLawn = true
                } else {
                    _x = _x + 1
                }
            }
            if outOfLawn {fmt.Println("Out of lawn, skip to next command !")
                continue
            }
            //deal the forward move check collision, check if the desired case is busy
            for _, mower := range (*p) {
                if mower.x == _x && mower.y == _y {
                    fmt.Println("Already busy, skip to next command !")
                    busySpace = true
                }
            }
            if busySpace {continue}
            //update mower position, lock value for other go routines reads in check collision step
            (*p)[id].mux.Lock()
            //time.Sleep(100 * time.Millisecond)
            (*p)[id].x =  _x
            (*p)[id].y =  _y
            (*p)[id].mux.Unlock()

        } else {
            fmt.Println("Command unknown");continue    
            }
        }
}

func main() {

mowers_slice, lawnAxes := readData()
wgChanCMDs.Wait()

for id := range mowers_slice {
    fmt.Println("start mower:",id)
    wgMowers.Add(1)
    go runMower(id, lawnAxes, &mowers_slice, &wgMowers)
}
wgMowers.Wait()

fmt.Println("Result =====")
for _, v := range mowers_slice {
    fmt.Println(v.x, v.y, v.orientation)
}

}